import React from 'react'

const Counter = (({numb}) => {
    return(
        <div className="counter">
            <h1>{numb}</h1>
        </div>
    )
});

export default Counter;