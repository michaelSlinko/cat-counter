import React from 'react';

const Buttons = ({sign, addCat, deleteCat}) => {
    if (sign === '+') {
        return (
            <button onClick={addCat}>Больше кить</button>
        );
    }
    else {
        return (
            <button onClick={deleteCat}>Меньше кить</button>
        );
    }
};

export default Buttons;