import React, {Component} from 'react';
import Counter from './counter';
import Buttons from './buttons';
import SetForm from './set-form';
import './app.css'

export default class App extends Component {
    state = {
        numb: 0
    };
    addCat = () => {
        const {numb} = this.state;
        this.setState(() => {
            return {
                numb: numb + 1
            }
        });
    };

    deleteCat = () => {
        const {numb} = this.state;
        this.setState(() => {
            if (numb <= 0){
                alert('Коты положительны!');
                return {numb: numb}
            }
            return {
                numb: numb - 1
            }
        });
    };

    setCat = (value) => {
        this.setState(() => {
            return {
                numb: parseInt(value)
            }
        });
    };

    render() {
        const {numb} = this.state;
        return (
            <div className="app">
                <Counter numb={numb}/>
                <SetForm setCat={this.setCat}/>
                <div className="buttons">
                    <Buttons sign='+' addCat={this.addCat}/>
                    <Buttons sign='-' deleteCat={this.deleteCat}/>
                </div>

            </div>
        );
    }
}