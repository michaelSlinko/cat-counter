import React, {Component} from 'react';

export default class SetForm extends Component {
    state = {
        value: 0
    };
    onChange = (e) => {
        const event = e.target.value;
        this.setState(() => {
            if (event.length > 0 && event > 0){
                return{
                    value: Math.abs(event)
                }
            }
            else if (event < 0) {
                alert('Коты положительны!');
                return {
                    value: Math.abs(event)
                }
            }
            return {
                value: 0
            }
        });
    };
    onSubmit = (e) => {
        e.preventDefault();
        this.props.setCat(this.state.value);
    };

    render() {
        return (
            <form onSubmit={this.onSubmit} className='setForm'>
                <input className='search' onChange={this.onChange} type='text' placeholder='Количество котиков'/>
                <button title='Нажмите с пустой строкой, чтобы обнулить значение.' className='set'>SET</button>
            </form>
        );
    }
}